<?php
namespace Common;
class AesSecurity{
	public static $aes_method="AES-128-ECB";//加密方法
	public static $key="aasadsa";//密鑰
	public static function encode($input){
		//aes encode
		$input=openssl_encrypt($input,static::$aes_method,static::$key,OPENSSL_RAW_DATA);	
		//base64 encode
		$input=base64_encode($input);	
		return $input;
	}	
	public static function decode($input){
		//base64 decode
		$input=base64_decode($input);	
		//aes decode
		$input=openssl_decrypt($input,static::$aes_method,static::$key,OPENSSL_RAW_DATA);
		return $input;
	}
	public static function test(){
		$source=[
			'utp'=>["A","S","M","E","C"][rand(0,4)],
			'uid'=>rand(1,10000000000000),
			'time'=>microtime(1),
		];
		$source=implode(",",$source);
		$encode=AesSecurity::encode($source);
		var_dump($encode);
		$decode=AesSecurity::decode($encode);
		// echo "<pre>";
		var_dump($decode);
		if($decode==$source){
			echo "\n";
		}else{
			throw new \Exception('GGWP');
		}
	}
}